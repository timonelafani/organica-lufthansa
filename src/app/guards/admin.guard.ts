import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  token;
  uid;
  constructor(private authService: AuthService, private router: Router) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    return this.authService.getUidFromToken ? this.authService.getUser(this.authService.getUidFromToken).pipe(map((data: any) => {
      if (data.role === 'admin' && this.authService.isLoggedIn) {
        return true;
      }
      this.router.navigate(['/unauthorized']);
      return false;
    })) : !this.authService.isLoggedIn &&
      this.router.navigateByUrl('/login');
  }


}
