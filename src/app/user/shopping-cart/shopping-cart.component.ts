import { Component, OnInit } from '@angular/core';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { UserService } from '../../services/user.service'
import { ProductsService } from '../../services/products.service';
import { AuthService } from '../../services/auth.service';
import { User } from '../../services/User';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {
  faTrashAlt = faTrashAlt
  products: string[] = []
  uid: User['uid']
  constructor(private userService: UserService, private productsService: ProductsService, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.getCartProducts()
    this.uid = this.authService.getUidFromToken
  }

  getProductsOnUpdate(products) {
    products.map(obj => {
      this.productsService.getProduct(obj.productId).subscribe(
        (product: any) => {
          const cartProduct = { ...product, totalPrice: obj.totalPrice, totalQuantity: obj.quantity }
          this.products = [...this.products, cartProduct]
        }
      )
    })
  }

  deleteProduct(product) {
    this.userService.deleteProductFromShoppingCart(product, this.uid).then(res => {
      res.subscribe(products => {
        this.products = [];
        return this.getProductsOnUpdate(products)
      })
    }
    )
  }

  getCartProducts() {
    this.userService.getUserCart().subscribe((res: any) => {
      this.getProductsOnUpdate(res)
    }
    )
  }

  buy(product) {
    this.userService.buyProduct(product, this.uid).then(res => {
      res.subscribe(products => {
        this.products = [];
        this.getProductsOnUpdate(products)
      })
    })
  }



}
