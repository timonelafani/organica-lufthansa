import { Component, OnInit } from '@angular/core';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { UserService } from '../../services/user.service'
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.css']
})
export class FavouritesComponent implements OnInit {
  faTrashAlt = faTrashAlt
  favouritesProducts: Object
  favourites: string[] = []
  constructor(private userService: UserService, private productsService: ProductsService) { }

  ngOnInit() {
    this.getFavourites()
  }

  getFavourites() {
    this.userService.getFavourites().subscribe(res => {
      this.favouritesProducts = res
    })
  }
  getProductsOnUpdate(products) {
    products.map(obj => {
      this.productsService.getProduct(obj.productId).subscribe(
        (product: any) => {
          const favouriteProduct = { ...product}
          this.favourites = [...this.favourites, favouriteProduct]
        }
      )
    })
  }
  deleteFavourite(productId) {
    this.userService.deleteFavourite(productId).then(res => {
      res.subscribe(products => {
        this.favourites = [];
        this.getProductsOnUpdate(products)
      })
    }
    )
  }
}
