import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { User } from '../../services/User'

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  buttonLabels = {
    buyHistory: 'Buy History',
    favourites: 'View Favourites',
    cart: 'Shopping cart'
  }
  constructor(private authService: AuthService) { }
  user: User
  ngOnInit() {
    this.getUser()
  }

  getUser() {
    this.authService.getUser(this.authService.getUidFromToken).subscribe((res: User) => {
      this.user = res
    })
  }

}
