import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, RouterOutlet } from '@angular/router';
import { FormControlsModule } from '../form-controls/form-controls.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { PinchZoomModule } from 'ngx-pinch-zoom';
import { FormsModule } from '@angular/forms';

import { FavouritesComponent } from './favourites/favourites.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ProductComponent } from './product/product.component';
import { UserGuard } from '../guards/user.guard';
import { BuyHistoryComponent } from './buy-history/buy-history.component';

const appRoutes: Routes = [
  { path: 'user/favourites', component: FavouritesComponent, canActivate: [UserGuard] },
  { path: 'user/shopping-cart', component: ShoppingCartComponent, canActivate: [UserGuard] },
  { path: 'user/profile', component: UserProfileComponent, canActivate: [UserGuard] },
  { path: 'product/:id', component: ProductComponent, canActivate: [UserGuard] },
  { path: 'user/buy-history', component: BuyHistoryComponent, canActivate: [UserGuard] }
];
@NgModule({
  declarations: [
    FavouritesComponent,
    ShoppingCartComponent,
    UserProfileComponent,
    ProductComponent,
    BuyHistoryComponent
  ],
  imports: [
    CommonModule,
    FormControlsModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FontAwesomeModule,
    PinchZoomModule,
    FormsModule
  ]
})
export class UserModule { }
