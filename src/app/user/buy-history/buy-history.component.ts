import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service'

@Component({
  selector: 'app-buy-history',
  templateUrl: './buy-history.component.html',
  styleUrls: ['./buy-history.component.css']
})
export class BuyHistoryComponent implements OnInit {
  boughtProducts: Object
  constructor(private userService: UserService, ) { }

  ngOnInit() {
    this.getProducts()
  }

  getProducts() {
    this.userService.getUserBoughtProducts().subscribe(res => {
      this.boughtProducts = res
    })
  }
}
