import { Component, OnInit, TemplateRef } from '@angular/core';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { Product } from '../../services/Product'
import { ProductsService } from '../../services/products.service'
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router'
import { UserService } from '../../services/user.service'
import { AuthService } from '../../services/auth.service';
import { map } from 'rxjs/operators';
import { NgForm } from '@angular/forms';
import { User } from '../../services/User';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal'

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  faHeart = faHeart
  pathUrl: string
  product: Product
  hasBoughtProduct: boolean
  comments: any[] = []
  modalRef: BsModalRef;
  productQuantity
  constructor(private productsService: ProductsService, private router: Router, private route: ActivatedRoute, private userService: UserService, private authService: AuthService, private modalService: BsModalService) {
    this.route.paramMap.subscribe((obj: any) => {
      this.pathUrl = obj.params.id
    });
  }

  ngOnInit() {
    this.getProduct()
    this.userHasBoughtProduct().subscribe((res) => { this.hasBoughtProduct = res })
    this.getAllComents(this.pathUrl)
  }


  getProduct() {
    this.productsService.getProduct(this.pathUrl)
      .subscribe((result: any) => {
        this.product = result
        this.productQuantity = {
          qty: 1,
          price: this.product.price
        }
      })
  }

  userHasBoughtProduct() {
    return this.userService.getUserBoughtProducts().pipe(map((res) => {
      let ids: string[] = []
      res.map(obj =>
        ids = [...ids, obj.id])
      if (ids.indexOf(this.pathUrl) > -1) {
        return true
      }
      else { return false }
    }))
  }

  comment = {
    text: ''
  }

  onAddComment(form: NgForm) {
    const payload = {
      text: this.comment.text,
    }

    this.userService.addComment({ ...payload, productId: this.pathUrl, uid: this.authService.getUidFromToken }).then(
      res => {
        this.comments = []
        this.getAllComents(this.pathUrl)

      }
    ).catch((error) => {
      window.alert(error.message)
      alert("Error!" + error.message);
    })
  }

  getAllComents(productId) {
    this.userService.getAllComments(productId).subscribe((res: any) => {
      res.map(comment => {
        this.authService.getUser(comment.uid).subscribe((user: any) => {
          this.comments.push({ comment: comment.comment, userImage: user.image, name: user.name })
        })
      })
    })
  }

  addToCart(payload) {
    const data = {
      productId: payload.productId,
      quantity: payload.quantity,
      totalPrice: payload.totalPrice
    }
    this.userService.addToCart(this.authService.getUidFromToken, data)
  }



  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  calculatePrice(qty) {
    const actualPrice = this.product.price
    const calculatedPrice = actualPrice * qty
    this.productQuantity.price = calculatedPrice
  }

  onAddToCart(form: NgForm) {
    const payload = {
      productId: this.pathUrl,
      totalPrice: this.product.price,
      quantity: this.productQuantity.qty,
    }
    if (this.productQuantity.qty >= 1 && this.productQuantity.qty <= this.product.quantity) {
      this.addToCart(payload)
      this.modalRef.hide()
    }
    else {
      alert("This quantity is not available")
    }
  }

}
