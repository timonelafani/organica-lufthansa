import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes, RouterOutlet } from '@angular/router';

//External modules
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from "@angular/fire/auth";
import { environment } from '../environments/environment';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AngularFireStorage } from '@angular/fire/storage';
//Services
import { AuthService } from "./services/auth.service";
//Guards
import { AdminGuard } from  './guards/admin.guard';
import { UserGuard } from  './guards/user.guard';
// Modules
import { AdminModule } from './admin/admin.module';
import { UserModule } from './user/user.module';
import { FormControlsModule } from './form-controls/form-controls.module';
//Components
import { AppComponent } from './app.component';
import { NavbarComponent } from './main/navbar/navbar.component';
import { HomeComponent } from './main/home/home.component';
import { FooterComponent } from './main/footer/footer.component';
import { LoginComponent } from './main/login/login.component';
import { ProductsListComponent } from './main/products-list/products-list.component';
import { PageNotFoundComponent } from './main/page-not-found/page-not-found.component';
import { UnauthorizedComponent } from './main/unauthorized/unauthorized.component';


const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'products', component: ProductsListComponent},
  { path: 'page-not-found', component: PageNotFoundComponent },
  { path: 'unauthorized', component: UnauthorizedComponent },
  { path: '**', redirectTo: '/page-not-found', pathMatch: 'full' },
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    FooterComponent,
    LoginComponent,
    ProductsListComponent,
    PageNotFoundComponent,
    UnauthorizedComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    FontAwesomeModule,
    AdminModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule,
    FormControlsModule,
    UserModule,
    ModalModule.forRoot()
  ],
  exports:[FormsModule],
  providers: [AuthService, AngularFireStorage],
  bootstrap: [AppComponent]
})
export class AppModule { }
