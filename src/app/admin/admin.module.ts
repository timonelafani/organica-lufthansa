import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { UsersComponent } from './users/users.component';
import { RouterModule, Routes, RouterOutlet } from '@angular/router';
import { SecondNavComponent } from './second-nav/second-nav.component';
import { ProductsComponent } from './products/products.component';
import { FormControlsModule } from '../form-controls/form-controls.module';
import { CreateEditUserComponent } from './create-edit-user/create-edit-user.component';
import { CreateEditProductComponent } from './create-edit-product/create-edit-product.component'
import { AdminGuard } from  '../guards/admin.guard';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

const appRoutes: Routes = [
  { path: 'admin/users', component: UsersComponent, canActivate: [AdminGuard] },
  { path: 'admin/products', component: ProductsComponent, canActivate: [AdminGuard] },
  { path: 'admin/create-product/:id', component: CreateEditProductComponent, canActivate: [AdminGuard] },
  { path: 'admin/create-user/:id', component: CreateEditUserComponent,canActivate: [AdminGuard] },
  {path: 'admin/create-user', redirectTo: 'admin/create-user/'},
  {path: 'admin/create-product', redirectTo: 'admin/create-product/'},
];

@NgModule({
  declarations: [UsersComponent, SecondNavComponent, ProductsComponent, CreateEditUserComponent, CreateEditProductComponent],
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes
    ),
    FormsModule,
    FormControlsModule,
    FontAwesomeModule
  ]
})
export class AdminModule { }
