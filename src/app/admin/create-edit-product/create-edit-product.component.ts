import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularFireStorage } from '@angular/fire/storage';
import { Product } from '../../services/Product'
import { ProductsService } from '../../services/products.service'
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-create-edit-product',
  templateUrl: './create-edit-product.component.html',
  styleUrls: ['./create-edit-product.component.css']
})
export class CreateEditProductComponent implements OnInit {
  ref
  task
  randomId
  downloadURL
  url
  pathUrl: string
  constructor(private afStorage: AngularFireStorage, private productsService: ProductsService, private router: Router, private route: ActivatedRoute) {
    this.route.paramMap.subscribe((obj: any) => {
      this.pathUrl = obj.params.id
    });
  }

  ngOnInit() {
    this.getProduct(this.pathUrl)
  }

  product: Product = {
    name: '',
    description: '',
    price: null,
    quantity: null,
    type: '',
    image: ''
  }

  getProduct(id) {
    this.productsService.getProduct(id).subscribe((res: Product) => {
      this.product.name = res.name
      this.product.description = res.description;
      this.product.price = res.price;
      this.product.quantity = res.quantity;
      this.product.type = res.type;
      this.product.image = res.image
    })
  }

  upload(event) {
    this.randomId = Math.random().toString(36).substring(2);
    this.task = this.afStorage.upload('users/' + this.randomId, event.target.files[0]).then(() => {
      const ref = this.afStorage.ref('users/' + this.randomId);
      this.downloadURL = ref.getDownloadURL().subscribe(url => {
        this.url = url
      })
    })
  }

  onCreateProduct(form: NgForm) {
    const payload = {
      name: this.product.name,
      description: this.product.description,
      type: this.product.type,
      price: this.product.price,
      quantity: this.product.quantity,
      image: this.url,
    }
    if (this.pathUrl) {
      this.productsService.updatePrduct({ ...payload, id: this.pathUrl }).then(
        res => {
          this.router.navigate(['/admin/products']);
        }
      ).catch((error) => {
        window.alert(error.message)
        alert("Error!" + error.message);
      })
    } else {
      this.productsService.addProduct(payload).then(
        res => {
          this.router.navigate(['/admin/products']);
        }
      ).catch((error) => {
        window.alert(error.message)
        alert("Error!" + error.message);
      })
    }

  }

}
