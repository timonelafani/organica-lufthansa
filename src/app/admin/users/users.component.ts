import { Component, OnInit, TemplateRef} from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal'
import { User } from '../../services/User';
import { UserService } from '../../services/user.service';
import { faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User
  faTrashAlt = faTrashAlt
  faEdit = faEdit
  modalRef: BsModalRef;
  constructor(private userService: UserService, private modalService: BsModalService, private router: Router) { }

  ngOnInit() {
    this.getUsers()
  }

  getUsers() {
    this.userService.getUsers()
      .subscribe((result: any) => {
        this.users = result.filter(user => user.uid)
      })
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  delete(id) {
    this.userService.deleteUser(id)
      .then(
        res => {
          this.router.navigate(['/admin/users']);
          this.modalRef.hide()
        },
        err => {
          alert(err)
        }
      )
  }

  edit(id){
    this.router.navigate([`/admin/create-user/${id}`]);
  }
}
