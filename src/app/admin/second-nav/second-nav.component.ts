import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-second-nav',
  templateUrl: './second-nav.component.html',
  styleUrls: ['./second-nav.component.css']
})
export class SecondNavComponent implements OnInit {
  list: any;
  selected: any;
  constructor() {
    this.list = [
      { title: 'Products', link: '/admin/products' },
      { title: 'Users', link: '/admin/users' }
    ];
  }
  select(item) {
    if (item === this.selected) {
      this.selected = null;
    } else {
      this.selected = item;
    }
  };
  isActive(item) {
    return this.selected === item;
  };
  ngOnInit() {
  }

}
