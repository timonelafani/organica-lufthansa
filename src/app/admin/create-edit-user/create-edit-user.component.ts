import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../services/auth.service";
import { NgForm } from '@angular/forms';
import { ProductsService } from '../../services/products.service'
import { ActivatedRoute } from '@angular/router';
import { User } from '../../services/User'

@Component({
  selector: 'app-create-edit-user',
  templateUrl: './create-edit-user.component.html',
  styleUrls: ['./create-edit-user.component.css']
})
export class CreateEditUserComponent implements OnInit {
  roles: Array<string> = ['user', 'admin']
  selectedRole: string
  urlUid: User['uid']
  constructor(public authService: AuthService, private prodService: ProductsService, private route: ActivatedRoute) {
    this.route.paramMap.subscribe((params: any) => {
      this.urlUid = params.params.id
    });
  }
  ngOnInit() {
    this.getUser(this.urlUid)
  }
  onChange(role) {
    this.selectedRole = role.toLowerCase()
  }

  user: User = {
    email: '',
    password: '',
    name: '',
    role: '',
    photoURL: null,
    uid: null
  }

  getUser(id) {
    this.authService.getUser(id).subscribe((res: User) => {
      this.user.email = res.email
      this.user.password = res.password
      this.user.name = res.name
      this.selectedRole = res.role
      this.user.photoURL = res.photoURL
      this.user.uid = res.uid
    })
  }

  onCreateUser(form: NgForm) {
    const payload = {
      email: this.user.email,
      password: this.user.password,
      name: this.user.name,
      role: this.selectedRole,
      photoURL: '../../assets/images/profile-pictures/profile-image-girl-2.jpg'
    }
    this.authService.register(payload)
  }

}
