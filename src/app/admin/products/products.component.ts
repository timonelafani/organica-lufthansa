import { Component, OnInit, EventEmitter, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal'
import { Product } from '../../services/Product'
import { ProductsService } from '../../services/products.service'
import { faTrashAlt, faEdit } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: Product
  faTrashAlt = faTrashAlt
  faEdit = faEdit
  modalRef: BsModalRef;
  name_filtered_items
  constructor(private productsService: ProductsService, private modalService: BsModalService, private router: Router) { }

  ngOnInit() {
    this.getProducts()
  }

  getProducts() {
    this.productsService.getProducts()
      .subscribe((result: any) => {
        this.products = result
        console.log(result)
      })
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  delete(id) {
    this.productsService.deleteProduct(id)
      .then(
        res => {
          this.router.navigate(['/admin/products']);
          this.modalRef.hide()
        },
        err => {
          alert(err)
        }
      )
  }

  search(event) {
    this.productsService.searchByName(event.target.value).subscribe((res: any) => {
      this.products = res
    }
    )
  }

  edit(id) {
    this.router.navigate([`/admin/create-product/${id}`]);
  }

}
