export interface Product {
    name: string;
    description: string;
    price: number;
    quantity: number;
    type: string;
    image:string
}