export interface User {
    uid: string;
    email: string;
    password: string;
    photoURL: string;
    role:  string;
    name:string
 }