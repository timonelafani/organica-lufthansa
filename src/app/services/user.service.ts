import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from './auth.service'
import { Router } from '@angular/router';
import { ProductsService } from './products.service';
import { Product } from './Product';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  isBuy: boolean
  constructor(public db: AngularFirestore, private authService: AuthService, private router: Router, private productsService: ProductsService) { }
  getUsers() {
    return this.db.collection('users').valueChanges();
  }

  deleteUser(uid) {
    return this.db.collection('users').doc(uid).delete();
  }



  addToCart(uid, payload) {
    return this.db.collection('cart').doc(uid).collection('user-cart').doc(payload.productId).set({
      productId: payload.productId,
      quantity: payload.quantity,
      totalPrice: payload.totalPrice
    }).then(() => {
      let product;
      this.productsService.getProduct(payload.productId).pipe(first()).subscribe((res: Product) => {
        product = {
          ...res,
          quantity: res.quantity - payload.quantity
        }
        this.productsService.updatePrduct(product)
      })
      alert('Product has been added to cart')
      this.router.navigate(['/user/shopping-cart'])
    })
  }

  getUserCart() {
    return this.db.collection('cart').doc(this.authService.getUidFromToken).collection('user-cart').valueChanges();
  }

  deleteProductFromShoppingCart(product, uid, isBuy?) {
    return this.db.collection('cart').doc(uid).collection('user-cart').doc(product.id).delete().then(() => {
      let producttoAdd;
      this.productsService.getProduct(product.id).pipe(first()).subscribe((res: Product) => {
        producttoAdd = {
          ...res,
          quantity: isBuy ? res.quantity : res.quantity + product.totalQuantity
        }
        this.productsService.updatePrduct(producttoAdd).then(() => this.getUserCart())
      })
      alert('Product has been removed from cart')
      isBuy ? this.router.navigate([`/user/buy-history`]) : this.router.navigate([`/product/${product.id}`])
      return this.getUserCart()
    });
  }

  buyProduct(product, uid) {
    this.isBuy = true
    return this.db.collection('bought').doc(uid).collection('ids').add({ ...product }).then(() => {
      return this.deleteProductFromShoppingCart(product, uid, this.isBuy)
    })
  }

  getUserBoughtProducts() {
    return this.db.collection('bought').doc(this.authService.getUidFromToken).collection('ids').valueChanges();
  }

  addComment(payload) {
    return this.db.collection('comments').doc(payload.productId).collection('comment').add({
      productId: payload.productId,
      uid: payload.uid,
      comment: payload.text
    })
  }

  getAllComments(productId) {
    return this.db.collection('comments').doc(productId).collection('comment').valueChanges();
  }

  addAsFavourite(payload) {
    return this.db.collection('favourites').doc(this.authService.getUidFromToken).collection('user-favourites').doc(payload.id).set({
      id: payload.id,
      image: payload.image,
      name: payload.name,
      price: payload.price,
      description: payload.description
    }).then(() => {
      alert('Product has been added to favourites')
      this.router.navigate(['/user/favourites'])
    }
    )
  }

  getFavourites() {
    return this.db.collection('favourites').doc(this.authService.getUidFromToken).collection('user-favourites').valueChanges();
  }

  deleteFavourite(productId) {
    return this.db.collection('favourites').doc(this.authService.getUidFromToken).collection('user-favourites').doc(productId).delete().then(() => {
      return this.getFavourites()
    });
  }
}
