import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(public db: AngularFirestore) {

  }
  addProduct(payload) {
    return this.db.collection('products').add({
      name: payload.name,
      description: payload.description,
      price: payload.price,
      quantity: payload.quantity,
      type: payload.type,
      image: payload.image,
      nametoLoweCase: payload.name.toLowerCase()
    }).then(res => {
      let product = {
        id: res.id
      }
      this.updatePrductId(product)
    });
  }

  private updatePrductId(product) {
    const productRef: AngularFirestoreDocument<any> = this.db.doc(`products/${product.id}`);
    const data = {
      id: product.id
    }
    return productRef.set(data, { merge: true })
  }

  updatePrduct(product) {
    const productRef: AngularFirestoreDocument<any> = this.db.doc(`products/${product.id}`);
    const data = {
      id: product.id,
      name: product.name,
      description: product.description,
      price: product.price,
      quantity: product.quantity,
      type: product.type,
      image: product.image,
      nametoLoweCase: product.name.toLowerCase()
    }
    return productRef.set(data, { merge: true })
  }

  getProducts() {
    return this.db.collection('products').valueChanges();
  }

  getProduct(id) {
    return this.db.doc(`products/${id}`).valueChanges();
  }


  deleteProduct(key) {
    console.log(key)
    return this.db.collection('products').doc(key).delete();
  }

  searchByName(searchValue) {
    return this.db.collection('products', ref => ref
      .orderBy("nametoLoweCase")
      .startAt(searchValue.toLowerCase())
      .endAt(searchValue.toLowerCase() + "\uf8ff")
      .limit(10))
      .valueChanges();
  }

}
