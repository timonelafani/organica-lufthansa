import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { User } from 'firebase';
import { Router } from "@angular/router";
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private user: Observable<User>;
  private userDetails: firebase.User = null;
  constructor(public afAuth: AngularFireAuth, public router: Router, public afs: AngularFirestore) {
    this.user = afAuth.authState;
    this.afAuth.authState.subscribe(user => {
      if (user) {
        user = user;
        this.userDetails = user;
        localStorage.setItem('user', JSON.stringify(this.userDetails));
      } else {
        this.userDetails = null;
        localStorage.setItem('user', null);
      }
    })
  }


  async  login(email: string, password: string) {
    try {
      await firebase.auth().onAuthStateChanged(user => {
        if (user) {
          this.getUserData(user.uid)
        }
      })
      await this.afAuth.auth.signInWithEmailAndPassword(email, password)
      this.router.navigate(['/products']);

    } catch (e) {
      alert("Error!" + e.message);
    }
  }

  getUserData(uid) {
    firebase.database().ref('users/' + uid).once("value", snap => {
    })
  }

  register(payload) {
    return this.afAuth.auth.createUserWithEmailAndPassword(payload.email, payload.password)
      .then((result) => {
        let user = {
          id: result.user.uid,
          email: result.user.email,
          role: payload.role,
          name: payload.name,
          file: payload.file
        }
        this.updateUserData(user)
        this.afs.collection('users').add(user).then(user => {
        }).catch(err => {
        })
      }).catch((error) => {
        window.alert(error.message)
        alert("Error!" + error.message);
      })
  }

  private updateUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.id}`);
    const data = {
      uid: user.id,
      email: user.email,
      role: 'user'
    }
    return userRef.set(data, { merge: true })
  }

  async logout() {
    await this.afAuth.auth.signOut();
    localStorage.removeItem('user');
    this.router.navigate(['/home']);
  }
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user !== null;
  }
  getUser(uid) {
    return this.afs.collection('users').doc(uid).valueChanges()
  }
  get getUidFromToken() {
    const token = JSON.parse(localStorage.getItem('user'))
    const uid = token && token.uid;
    return uid
  }

}
