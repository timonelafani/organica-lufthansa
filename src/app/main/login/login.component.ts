import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

interface user {
  email: string;
  password: string
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  constructor(
    private router: Router, private authService: AuthService) { }

  ngOnInit() {
  }

  user: user = {
    email: '',
    password: ''
  }

  submitted = false;

  onSignin(form: NgForm) {
    this.submitted = true;
    const payload = {
      email: this.user.email,
      password: this.user.password,
    }
    this.authService.login(payload.email, payload.password)
  }



}
