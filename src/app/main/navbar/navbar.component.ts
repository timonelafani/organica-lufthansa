import { Component, OnInit, HostListener, Inject } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { DOCUMENT } from '@angular/common';
import { faShoppingCart, faStream } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from '../../services/auth.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  animations: [
    trigger('fade',
      [
        state('void', style({ opacity: 0 })),
        transition(':enter', [animate(300)]),
        transition(':leave', [animate(500)]),
      ]
    )]
})
export class NavbarComponent implements OnInit {
  isAdmin: boolean
  constructor(@Inject(DOCUMENT) document, private authService: AuthService) {
    this.isLoggedUserAdmin()
   }
  faShoppingCart = faShoppingCart;
  faStream = faStream
  ngOnInit() {
    this.isLoggedUserAdmin()
  }

  isLoggedUserAdmin() {
    return this.authService.getUidFromToken && this.authService.getUser(this.authService.getUidFromToken).subscribe((data: any) => {
      if (data && data.role === 'admin' && this.authService.isLoggedIn) {
        return this.isAdmin = true;
      }
      else return this.isAdmin = false
    })
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
    if (window.pageYOffset > 100) {
      let element = document.getElementById('navbar');
      element.classList.add('sticky');
    } else {
      let element = document.getElementById('navbar');
      element.classList.remove('sticky');
    }
  }
}
