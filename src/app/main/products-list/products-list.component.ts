import { Component, OnInit } from '@angular/core';
import { faHeart, faShoppingCart, faArrowsAltH } from '@fortawesome/free-solid-svg-icons';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal'
import { Product } from '../../services/Product'
import { ProductsService } from '../../services/products.service'
import { Router } from '@angular/router';
import { UserService } from '../../services/user.service'
import { User } from '../../services/User'
import { AuthService } from '../../services/auth.service'
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {
  products: Product
  faHeart = faHeart;
  faShoppingCart = faShoppingCart;
  faArrowsAltH = faArrowsAltH;
  isLogged: boolean
  uid: User['uid']
  isAdmin: boolean
  constructor(private productsService: ProductsService, private modalService: BsModalService, private router: Router, private userService: UserService, private authService: AuthService) { }

  ngOnInit() {
    this.getProducts()
    this.uid = this.authService.getUidFromToken
    this.isLogged = this.authService.isLoggedIn
    this.isLoggedUserAdmin()
  }

  isLoggedUserAdmin() {
    return this.authService.getUser(this.authService.getUidFromToken).subscribe((data: any) => {
      if (data && data.role === 'admin' && this.authService.isLoggedIn) {
        return this.isAdmin = true;
      }
      else return this.isAdmin = false
    })
  }

  getProducts() {
    this.productsService.getProducts()
      .subscribe((result: any) => {
        this.products = result
      })
  }

  viewProduct(id) {
    if (this.isLogged && !this.isAdmin) {
      this.router.navigate([`/product/${id}`]);
    } else if (this.isLogged && this.isAdmin) {
      alert('You must have a user account')
    }
    else {
      alert('You must be logged in')
      this.router.navigate(['/login'])
    }

  }

  addAsFavourite(product) {
    if (this.isLogged && !this.isAdmin) {
      this.userService.addAsFavourite(product)
    } else if (this.isLogged && this.isAdmin) {
      alert('You must have a user account')
    }
    else {
      alert('You must be logged in')
      this.router.navigate(['/login'])
    }
  }

  isAddedAsFavourite(product) {
    let favouritesIds = []
    this.userService.getFavourites().pipe(first()).subscribe(res => {
      res.length > 0 ? res.map(obj => {
        favouritesIds = [...favouritesIds, obj.id]
      }) : favouritesIds
      favouritesIds.indexOf(product.id) >= 0 ? alert('Product has already been added') : this.addAsFavourite(product)
    })
  }

}
