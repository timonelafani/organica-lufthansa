import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  @Input() class: string;
  @Output() onClick = new EventEmitter<any>();

 
  onButtonClick(event) {
    this.onClick.emit(event);
  }
}
