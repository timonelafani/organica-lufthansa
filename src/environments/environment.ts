// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCUhFPVTcslnY3e-Y_MLmPx8iYoh-TbR0g",
    authDomain: "my-project-1550617491096.firebaseapp.com",
    databaseURL: "https://my-project-1550617491096.firebaseio.com",
    projectId: "my-project-1550617491096",
    storageBucket: "my-project-1550617491096.appspot.com",
    messagingSenderId: "951410044575",
    appId: "1:951410044575:web:41e0668eccb9b60d8cfcaa",
    measurementId: "G-VB6W7B2KEG"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
